package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        if (salary >= 50000) {
            this.name = name;
            this.salary = salary;
            this.role = "Network Expert";
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
