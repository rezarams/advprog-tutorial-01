package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary) {
        if (salary >= 70000) {
            this.name = name;
            this.salary = salary;
            this.role = "Security Expert";
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
