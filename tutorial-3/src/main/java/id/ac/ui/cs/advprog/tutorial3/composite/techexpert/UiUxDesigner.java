package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary) {
        if (salary >= 90000) {
            this.name = name;
            this.salary = salary;
            this.role = "UI/UX Designer";
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
