package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.List;

public class CompMain {
    public static void main(String[] args) {
        Company company = new Company();
        Ceo test1 = new Ceo("Luffy", 510000.00);
        company.addEmployee(test1);
        Cto test2 = new Cto("Zorro", 323000.00);
        company.addEmployee(test2);
        BackendProgrammer test3 = new BackendProgrammer("Franky", 104000.00);
        company.addEmployee(test3);
        FrontendProgrammer test4 = new FrontendProgrammer("Nami",69000.00);
        company.addEmployee(test4);
        UiUxDesigner test5 = new UiUxDesigner("Sanji", 176000.00);
        company.addEmployee(test5);
        NetworkExpert test6 = new NetworkExpert("Brook", 85000.00);
        company.addEmployee(test6);
        SecurityExpert test7 = new SecurityExpert("Chopper", 81000.00);
        company.addEmployee(test7);
        List<Employees> allEmployees = company.getAllEmployees();
        allEmployees.stream().forEach(i -> System.out.println(i.getRole() + " "
                + i.getName() + " dengan gaji " + i.getSalary()));
        System.out.println("Gaji total: " + company.getNetSalaries());
    }
}