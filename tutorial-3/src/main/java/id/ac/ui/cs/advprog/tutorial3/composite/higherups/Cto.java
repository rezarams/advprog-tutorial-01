package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        //TODO Implement
        if (salary >= 100000) {
            this.name = name;
            this.salary = salary;
            this.role = "CTO";
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
